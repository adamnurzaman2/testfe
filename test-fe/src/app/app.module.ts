import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { ProfileComponent } from './profile/profile.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { FirsttestComponent } from './firsttest/firsttest.component';

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    HomeComponent,
    SearchComponent,
    FirsttestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
	ReactiveFormsModule,
	HttpClientModule,
	NgxPaginationModule,
	AgmCoreModule.forRoot({
		apiKey:'AIzaSyDOyIpJu9REAYg1gjuogfdBA3jMVIycndA'
	})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
