import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-firsttest',
  templateUrl: './firsttest.component.html',
  styleUrls: ['./firsttest.component.css']
})
export class FirsttestComponent {
  input1 = new FormControl('');
  hasil1 = new FormControl('');
  input2 = new FormControl('');
  hasil2 = new FormControl('');
  
  
	calculateFactorial(num) {
	  if (num < 0) {
        return -1;
      } else if (num === 1) {
		return 1;
	  } else {
		return num * this.calculateFactorial(num - 1);
	  }
	}
	
	show1() {
		var num = this.input1.value;
		this.hasil1.setValue(this.calculateFactorial(num));
	}
  
	show2() {
		var exp = this.input2.value;
		var expCounts = {};
		var maxKey = '';
		for(var i = 0; i < exp.length; i++)
		{
			var key = exp[i];
			if(!expCounts[key]){
			 expCounts[key] = 0;
			}
			expCounts[key]++;
			if(maxKey == '' || expCounts[key] > expCounts[maxKey]){
				maxKey = key;
			}
		}
		this.hasil2.setValue(maxKey);
	}
}
