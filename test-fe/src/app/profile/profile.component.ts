import { Component, OnInit } from '@angular/core';
import { AgmCoreModule } from '@agm/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
	
	latitude = -28.68352;
	longitude = -147.20785;
	mapType = 'satellite';

  constructor() { }

  ngOnInit(): void {
  }

}
