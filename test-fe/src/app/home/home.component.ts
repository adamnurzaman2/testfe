import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  //constructor(private _Home: HomeService) { }

	constructor(private http : HttpClient){

   } 
  ngOnInit(): void {
	//  this._Home.getAll()
	//  .subscribe(data => {
	//	this.locations = data;
	//	console.log(this.locations);
	//  });
	this.http.get('http://52.76.85.10/test/location.json').subscribe(data => {
		//this.locations = data;
		//console.log(this.locations);
	  });
  }

}
