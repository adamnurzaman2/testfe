import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }

  public getJSON(): Observable<any> {
    return this.http.get("http://52.76.85.10/test/datalist.json");
  }

}
