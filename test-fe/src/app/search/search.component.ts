import { Component, OnInit } from '@angular/core';
import { SearchService } from './search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  public data: any[] = [];
  constructor(private _Search: SearchService) {}

	curPage: number;
	pageSize: number;
  ngOnInit(): void {
    this._Search.getJSON().subscribe(response => {
      this.data = response;
    });
	
	this.curPage = 1;
   this.pageSize = 10;
  }
  
  numberOfPages() {
   return Math.ceil(this.data.length / this.pageSize);
 };

}
